package com.freakymobapps.tpo_fashionista.FragmentActivities;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.freakymobapps.tpo_fashionista.CallBack;
import com.freakymobapps.tpo_fashionista.JSON;
import com.freakymobapps.tpo_fashionista.Post;
import com.freakymobapps.tpo_fashionista.PostAdapter;
import com.freakymobapps.tpo_fashionista.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by 633k on 7.1.2017.
 */

public class FragmentAllPosts extends Fragment {

    //tags
    private static final String TAG_ID = "ID";
    private static final String TAG_POSTS = "posts";
    private static final String TAG_USERNAME = "user";
    private static final String TAG_STR_PIC = "picture";
    private static final String TAG_DESCRIPTION = "comment";
    private static final String TAG_STYLE = "style";
    private static final String TAG_LIKE = "loveit";
    private static final String TAG_HATE = "hateit";
    private static final String TAG_USERLIKE = "userlove";
    private static final String TAG_USERHATE = "userhate";
    private static final String TAG_USERPOST_ID = "IDuser";
    private static final String TAG_DATE = "date";


    // An array of all of our posts
    private JSONArray mComments = null;
    private ArrayList<Post> posts;

    private int id;
    ListView list;
    private SwipeRefreshLayout swipeContainer;

    public static final FragmentAllPosts newInstance() {
        FragmentAllPosts f = new FragmentAllPosts();
        Bundle b = new Bundle();
        f.setArguments(b);

        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.frag_all_posts, container, false);
        //get data from shared prefs
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MyProfile", MODE_PRIVATE);
        id = sharedPreferences.getInt("id", -1);

        list = (ListView) view.findViewById(R.id.allList);

        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPosts();
            }
        });

        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                getPosts();
            }
        });


        return view;
    }

    void getPosts() {
        swipeContainer.setRefreshing(true);
        JSON j = new JSON(getContext(), new CallBack() {
            @Override
            public void getResult(String name, JSONObject jo) {
                if (name.equals("getAllPosts")) {
                    swipeContainer.setRefreshing(false);
                    if (jo != null) {
                        try {
                            JSONArray jaPosts = jo.getJSONArray(TAG_POSTS);
                            parsePosts(jaPosts);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        j.post("getAllPosts", "getAllPosts.php", "ID_up="+id);
    }

    private void parsePosts(JSONArray jaPosts) {
        posts = new ArrayList<>();
        for (int i = 0; i < jaPosts.length(); i++) {
            try {
                JSONObject joRow = jaPosts.getJSONObject(i);


                int id = joRow.getInt(TAG_ID);
                String username = joRow.getString(TAG_USERNAME);
                String strPic = joRow.getString(TAG_STR_PIC);
                String strProfilna = joRow.getString("profilna");
                String description = joRow.getString(TAG_DESCRIPTION);
                String style = joRow.getString(TAG_STYLE);
                int likes = joRow.getInt(TAG_LIKE);
                int hates = joRow.getInt(TAG_HATE);
                int userlike = joRow.getInt(TAG_USERLIKE);
                int userhate = joRow.getInt(TAG_USERHATE);
                int userpostID = joRow.getInt(TAG_USERPOST_ID);
                Bitmap bitmapPic = StringToBitMap(strPic);
                Bitmap profilePic = StringToBitMap(strProfilna);
                String date = joRow.getString(TAG_DATE);


                Post p = new Post(id, username, description, style, likes, hates, userlike, userhate, userpostID, bitmapPic, profilePic, date);

                posts.add(p);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        setAdapter();


    }

    private void setAdapter() {
        list.setAdapter(new PostAdapter(getContext(), posts, id));
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

}

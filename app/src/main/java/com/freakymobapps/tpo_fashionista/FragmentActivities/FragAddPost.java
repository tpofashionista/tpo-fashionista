package com.freakymobapps.tpo_fashionista.FragmentActivities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by 633k on 9.1.2017.
 */

public class FragAddPost extends Fragment {
    public static final FragAddPost newInstance() {
        FragAddPost f = new FragAddPost();

        Bundle b = new Bundle();
        f.setArguments(b);

        return f;
    }
}

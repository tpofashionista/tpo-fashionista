package com.freakymobapps.tpo_fashionista.FragmentActivities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freakymobapps.tpo_fashionista.CallBack;
import com.freakymobapps.tpo_fashionista.JSON;
import com.freakymobapps.tpo_fashionista.Post;
import com.freakymobapps.tpo_fashionista.PostAdapter;
import com.freakymobapps.tpo_fashionista.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static android.R.id.list;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by 633k on 8.1.2017.
 */

public class MyProfile extends Fragment {



    private static final String READ_COMMENTS_URL = "http://efashionista.tk/getposts.php";
    private static final String TAG_ID = "ID";
    private static final String TAG_USERNAME = "user";
    private static final String TAG_POSTS = "posts";
    private static final String TAG_STR_PIC = "picture";
    private static final String TAG_DESCRIPTION = "comment";
    private static final String TAG_STYLE = "style";
    private static final String TAG_LIKE = "loveit";
    private static final String TAG_HATE = "hateit";
    private static final String TAG_USERLIKE = "userlove";
    private static final String TAG_USERHATE = "userhate";
    private static final String TAG_USERPOST_ID = "IDuser";
    private static final String TAG_DATE = "date";

    private static final int CAMERA_PIC_REQUEST = 1;
    final int PIC_CROP = 2;
    private Uri picUri;
    private String strNewPic;
    private Bitmap bitmapNew;

    private int id;
    ListView list;
    private SwipeRefreshLayout swipeContainer;
    private ArrayList<Post> posts;

    ImageView imgProfilePic;
    TextView tvUsername;
    TextView tvNumPosts;
    TextView tvNumFollowers;
    TextView tvNumFollowing;

    LinearLayout lnPosts;
    LinearLayout lnFollowers;
    LinearLayout lnFollowing;


    public static final MyProfile newInstance() {
        MyProfile f = new MyProfile();

        Bundle b = new Bundle();
        f.setArguments(b);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.activity_my_profile, container, false);

        //get data from shared prefs
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MyProfile", MODE_PRIVATE);
        id = sharedPreferences.getInt("id", -1);


        list = (ListView) view.findViewById(R.id.myProfilePostList);
        tvUsername = (TextView) view.findViewById(R.id.tvUsername);
        tvNumPosts = (TextView) view.findViewById(R.id.tvNumPosts);
        tvNumFollowers = (TextView) view.findViewById(R.id.tvNumFollowers);
        tvNumFollowing= (TextView) view.findViewById(R.id.tvNumFollowing);
        imgProfilePic = (ImageView) view.findViewById(R.id.imgMyProfile);

        lnPosts = (LinearLayout) view.findViewById(R.id.lnNumPosts);
        lnFollowers = (LinearLayout) view.findViewById(R.id.lnNumFollowers);
        lnFollowing = (LinearLayout) view.findViewById(R.id.lnNumFollowing);

        lnFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFollowers();
            }
        });

        lnFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFollowing();
            }
        });

        imgProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Change picture", Toast.LENGTH_SHORT).show();
            }
        });


        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPosts();
            }
        });

        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                getPosts();
            }
        });

        return view;

    }

    private void showFollowing() {
        Toast.makeText(getContext(), "show following dialog", Toast.LENGTH_SHORT).show();
        JSON j = new JSON(getContext(), new CallBack() {
            @Override
            public void getResult(String name, JSONObject jo) {

            }
        });
        //j.post("getFollowing", "", "");
    }

    private void showFollowers() {

        Toast.makeText(getContext(), "show followers dialog", Toast.LENGTH_SHORT).show();

        JSON j = new JSON(getContext(), new CallBack() {
            @Override
            public void getResult(String name, JSONObject jo) {

            }
        });
        //j.post("getFollowers", "", "");
    }

    void getPosts() {
        JSON j2 = new JSON(getContext(), new CallBack() {
            @Override
            public void getResult(String name, JSONObject jo) {
                if (name.equals("getProfile")) {
                    //System.out.println("get profile" + jo.toString());
                    if (jo != null) {
                        try {

                            tvUsername.setText(jo.getString("user"));
                            int numPosts = jo.getInt("numposts");
                            int numFollowers = jo.getInt("followed");
                            int numFollowing = jo.getInt("following");
                            String strPic = jo.getString("profpicture");
                            imgProfilePic.setImageBitmap(StringToBitMap(strPic));
                            System.out.println(numFollowers);
                            tvNumPosts.setText(numPosts+"");
                            tvNumFollowers.setText(numFollowers+"");
                            tvNumFollowing.setText(numFollowing+"");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        j2.post("getProfile", "getProfile.php", "ID_up="+id);
        JSON j = new JSON(getContext(), new CallBack() {
            @Override
            public void getResult(String name, JSONObject jo) {
                if (name.equals("myPosts")) {
                    swipeContainer.setRefreshing(false);
                    if (jo != null) {
                        try {
                            JSONArray jaPosts = jo.getJSONArray(TAG_POSTS);
                            parsePosts(jaPosts);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        j.post("myPosts", "getPosts.php", "ID_up="+id + "&ID_viewp="+id);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_PIC_REQUEST) {
                picUri = data.getData();
                performCrop();
            } else if (requestCode == PIC_CROP) {
                bitmapNew = data.getParcelableExtra("data");
                strNewPic = BitMapToString(bitmapNew);
                //TODO: upload picture

                //new PostPicturePHP().execute();
            }
        } else {
            //finish();
        }
    }
    private void performCrop() {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 100);
            cropIntent.putExtra("outputY", 100);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }


    private void parsePosts(JSONArray jaPosts) {
        posts = new ArrayList<>();
        for (int i = 0; i < jaPosts.length(); i++) {
            try {
                JSONObject joRow = jaPosts.getJSONObject(i);


                int id = joRow.getInt(TAG_ID);
                String username = joRow.getString(TAG_USERNAME);
                String strPic = joRow.getString(TAG_STR_PIC);
                String strProfilna = joRow.getString("profilna");
                String description = joRow.getString(TAG_DESCRIPTION);
                String style = joRow.getString(TAG_STYLE);
                int likes = joRow.getInt(TAG_LIKE);
                int hates = joRow.getInt(TAG_HATE);
                int userlike = joRow.getInt(TAG_USERLIKE);
                int userhate = joRow.getInt(TAG_USERHATE);
                int userpostID = joRow.getInt(TAG_USERPOST_ID);
                Bitmap bitmapPic = StringToBitMap(strPic);
                Bitmap profilePic = StringToBitMap(strProfilna);
                String date = joRow.getString(TAG_DATE);


                Post p = new Post(id, username, description, style, likes, hates, userlike, userhate, userpostID, bitmapPic, profilePic, date);
                posts.add(p);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        setAdapter();
    }
    private void setAdapter() {
        list.setAdapter(new PostAdapter(getContext(), posts, id));
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }
}

package com.freakymobapps.tpo_fashionista.FragmentActivities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.freakymobapps.tpo_fashionista.CallBack;
import com.freakymobapps.tpo_fashionista.JSON;
import com.freakymobapps.tpo_fashionista.Post;
import com.freakymobapps.tpo_fashionista.PostAdapter;
import com.freakymobapps.tpo_fashionista.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by 633k on 9.1.2017.
 */

public class OtherProfile extends Activity implements CallBack, View.OnClickListener {

    private ImageView imgProfile;
    private TextView tvUsername;
    private TextView tvPosts;
    private TextView tvFollowing;
    private TextView tvFollowers;
    private ToggleButton tglBtnFollow;
    private ListView list;
    private SwipeRefreshLayout swipeContainer;

    private static final String TAG_ID = "ID";
    private static final String TAG_USERNAME = "user";
    private static final String TAG_POSTS = "posts";
    private static final String TAG_STR_PIC = "picture";
    private static final String TAG_DESCRIPTION = "comment";
    private static final String TAG_STYLE = "style";
    private static final String TAG_LIKE = "loveit";
    private static final String TAG_HATE = "hateit";
    private static final String TAG_USERLIKE = "userlove";
    private static final String TAG_USERHATE = "userhate";
    private static final String TAG_USERPOST_ID = "IDuser";
    private static final String TAG_DATE = "date";

    private ArrayList<Post> posts;

    int hisId;
    int myId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_profile);

        Intent intent = getIntent();
        hisId = intent.getIntExtra("otherID", -1);


        //get data from shared prefs
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyProfile", MODE_PRIVATE);
        myId = sharedPreferences.getInt("id", -1);

        System.out.println("my: " + myId + ", his: " + hisId);

        init();
    }

    private void init() {
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        tvUsername = (TextView) findViewById(R.id.tvUsername);
        tvPosts = (TextView) findViewById(R.id.tvPosts);
        tvFollowing = (TextView) findViewById(R.id.tvFollowing);
        tvFollowers = (TextView) findViewById(R.id.tvFollowers);
        tglBtnFollow = (ToggleButton) findViewById(R.id.tglBtnOtherProfile);

        tglBtnFollow.setEnabled(false);

        list = (ListView) findViewById(R.id.listOtherPosts);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPosts();
                getProfileInfo();
            }
        });

        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                getPosts();
                getProfileInfo();
            }
        });

    }

    private void unfollow() {
        JSON j = new JSON(this);
        j.post("unfollow", "follow.php", "ID_a="+myId+"&ID_b="+hisId);
        int n = Integer.parseInt(tvFollowers.getText().toString())-1;
        tvFollowers.setText(String.valueOf(n));
    }

    private void follow() {
        JSON j = new JSON(this);
        j.post("follow", "follow.php", "ID_a="+myId+"&ID_b="+hisId);
        int n = Integer.parseInt(tvFollowers.getText().toString())+1;
        tvFollowers.setText(String.valueOf(n));
    }

    private void getProfileInfo() {
        swipeContainer.setRefreshing(true);
        JSON j = new JSON(this);
        j.post("getProfile", "getProfile.php", "ID_up=" + hisId + "&ID=" + myId);
    }

    void getPosts() {
        swipeContainer.setRefreshing(true);
        JSON j = new JSON(this);
        j.post("myPosts", "getPosts.php", "ID_up=" + hisId + "&ID_viewp=" + hisId);
    }

    private void parsePosts(JSONArray jaPosts) {
        posts = new ArrayList<>();
        for (int i = 0; i < jaPosts.length(); i++) {
            try {
                JSONObject joRow = jaPosts.getJSONObject(i);


                int id = joRow.getInt(TAG_ID);
                String username = joRow.getString(TAG_USERNAME);
                String strPic = joRow.getString(TAG_STR_PIC);
                String strProfilna = joRow.getString("profilna");
                String description = joRow.getString(TAG_DESCRIPTION);
                String style = joRow.getString(TAG_STYLE);
                int likes = joRow.getInt(TAG_LIKE);
                int hates = joRow.getInt(TAG_HATE);
                int userlike = joRow.getInt(TAG_USERLIKE);
                int userhate = joRow.getInt(TAG_USERHATE);
                int userpostID = joRow.getInt(TAG_USERPOST_ID);
                Bitmap bitmapPic = StringToBitMap(strPic);
                Bitmap profilePic = StringToBitMap(strProfilna);
                String date = joRow.getString(TAG_DATE);


                Post p = new Post(id, username, description, style, likes, hates, userlike, userhate, userpostID, bitmapPic, profilePic, date);
                posts.add(p);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        setAdapter();
    }

    @Override
    public void getResult(String name, JSONObject jo) {
        if (jo != null) {
            if (name.equals("myPosts")) {
                try {
                    JSONArray jaPosts = jo.getJSONArray(TAG_POSTS);
                    parsePosts(jaPosts);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (name.equals("getProfile")) {
                swipeContainer.setRefreshing(false);
                System.out.println(jo.toString());
                try {
                    String u = jo.getString("user");
                    tvUsername.setText(u);
                    int numPosts = jo.getInt("numposts");
                    int numFollowers = jo.getInt("followed");
                    int numFollowing = jo.getInt("following");
                    String strPic = jo.getString("picture");
                    imgProfile.setImageBitmap(StringToBitMap(strPic));
                    tvPosts.setText(numPosts+"");
                    tvFollowers.setText(numFollowers+"");
                    tvFollowing.setText(numFollowing+"");
                    boolean iFollow = jo.getInt("ifollow") == 1 ? true: false;
                    tglBtnFollow.setEnabled(true);
                    tglBtnFollow.setChecked(iFollow);

                    tglBtnFollow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (b)
                                follow();
                            else
                                unfollow();
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View view) {

    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private void setAdapter() {
        list.setAdapter(new PostAdapter(this, posts, hisId));
    }
}

package com.freakymobapps.tpo_fashionista;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.freakymobapps.tpo_fashionista.FragmentActivities.FragmentAllPosts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class FragmentAddPost extends Fragment{

    //Camera
    final int CAMERA_CAPTURE = 1;
    final int PIC_CROP = 2;
    private Uri picUri;
    String pictureString;

    //PHP
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    //views
    private ImageView imgPreview;
    private Spinner spinner;
    private EditText etDescription;
    private Button btnPost;

    //variables
    private int id;
    private String strStyle;
    private String strDescription;
    private Bitmap bitmapPicture;

    View view;

    public static boolean isOnScreen = false;


    public static final FragmentAddPost newInstance(boolean bool) {
        FragmentAddPost f = new FragmentAddPost();

        isOnScreen = bool;
        Bundle b = new Bundle();
        f.setArguments(b);

        return f;
    }

/*
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        view = inflater.inflate(R.layout.activity_add_post, container, false);

        //get data from shared prefs
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("MyProfile", MODE_PRIVATE);
        id = sharedPreferences.getInt("id", -1);


        if (isOnScreen) {
            try {
                //use standard intent to capture an image
                Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //we will handle the returned data in onActivityResult
                startActivityForResult(captureIntent, CAMERA_CAPTURE);
            } catch (ActivityNotFoundException anfe) {
                //display an error message
                String errorMessage = "Your device doesn't support capturing images!";
                Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        }
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        System.out.println("on resume");
        if (isOnScreen) {
            isOnScreen = false;
            try {
                //use standard intent to capture an image
                Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //we will handle the returned data in onActivityResult
                startActivityForResult(captureIntent, CAMERA_CAPTURE);
            } catch (ActivityNotFoundException anfe) {
                //display an error message
                String errorMessage = "Your device doesn't support capturing images!";
                Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CAPTURE) {
                picUri = data.getData();
                performCrop();
            } else if (requestCode == PIC_CROP) {
                bitmapPicture = data.getParcelableExtra("data");
                //ImageView picView = (ImageView) findViewById(R.id.post_small_img);
                //display the returned cropped image
                pictureString = BitMapToString(bitmapPicture);
            }
        } else {
            Intent i = new Intent(getContext(), MainActivity.class);
            startActivity(i);
            //finish();
        }


        //initialize components
        init();
    }


    private void init() {
        imgPreview = (ImageView) view.findViewById(R.id.imgPreview);
        spinner = (Spinner) view.findViewById(R.id.spnrStyles);
        etDescription = (EditText) view.findViewById(R.id.etComment);
        btnPost = (Button) view.findViewById(R.id.btnUpload);

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //post
                strStyle = spinner.getSelectedItem().toString();
                strDescription = etDescription.getText().toString();
                JSON j = new JSON(FragmentAddPost.this);
                String data = null;
                try {
                    data = getUrlEncodedText(pictureString, id, strStyle, strDescription);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                j.postProgress("upload", "addPost.php", data);
            }
        });


        //TODO: later read styles from database
        ArrayList<String> array = new ArrayList<>();
        array.add("Night");
        array.add("Party");
        array.add("Casual");
        array.add("Elegant");
        array.add("Hip-Hop");
        array.add("Rock");
        array.add("Pop");
        array.add("Classy");
        array.add("Metal");
        array.add("Other");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(0);

        //set imageView to picture from camera
        imgPreview.setImageBitmap(bitmapPicture);
    }

    @Override
    public void getResult(String name, JSONObject jo) {
        if (name.equals("upload")) {
            int success = 0;
            try {
                success = jo.getInt("success");
                if (success == 1) {
                    Toast.makeText(getContext(), "Post successfully added!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getContext(), MainActivity.class));
                } else {
                    Toast.makeText(getContext(), "Posting picture Faliure!", Toast.LENGTH_SHORT).show();
                    System.out.println("Posting picture Faliure!" + jo.getString(TAG_MESSAGE));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void performCrop() {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image*//*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 100);
            cropIntent.putExtra("aspectY", 100);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 500);
            cropIntent.putExtra("outputY", 500);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Your device doesn't support the crop action!";
            Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    private String getUrlEncodedText(String pictureString, int user, String style, String comment) throws UnsupportedEncodingException {
        String data = URLEncoder.encode("pictureString", "UTF-8")
                + "=" + URLEncoder.encode(pictureString, "UTF-8");

        data += "&" + URLEncoder.encode("username", "UTF-8") + "="
                + URLEncoder.encode(String.valueOf(user), "UTF-8");

        data += "&" + URLEncoder.encode("style", "UTF-8") + "="
                + URLEncoder.encode(style, "UTF-8");

        data += "&" + URLEncoder.encode("comment", "UTF-8")
                + "=" + URLEncoder.encode(comment, "UTF-8");
        return data;
    }*/
}

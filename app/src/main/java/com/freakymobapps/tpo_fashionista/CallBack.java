package com.freakymobapps.tpo_fashionista;

import org.json.JSONObject;

/**
 * Created by 633k on 1.12.2016.
 */
public interface CallBack {
    //interface with attributes name for action name and jsonObject
    //Result is in function getResult with the same name
    void getResult(String name, JSONObject jo);
}

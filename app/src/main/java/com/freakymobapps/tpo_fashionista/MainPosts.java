package com.freakymobapps.tpo_fashionista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.Toast;

import com.freakymobapps.tpo_fashionista.FragmentActivities.FragmentAllPosts;
import com.freakymobapps.tpo_fashionista.FragmentActivities.FragmentFollowersPosts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by 633k on 7.1.2017.
 */

public class MainPosts extends FragmentActivity implements TabHost.OnTabChangeListener, CallBack {

    private TabHost mTabHost;
    private HashMap mapTabInfo = new HashMap();
    private TabInfo mLastTab = null;

    //data
    private int id;
    private boolean isLogin = false;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                Toast.makeText(MainPosts.this, "Here comes the about dialog!", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_logout:
                SharedPreferences sharedPreferences = this.getSharedPreferences("MyProfile", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("isLogin", false);
                editor.putInt("id", -1);
                editor.commit();

                startActivity(new Intent(MainPosts.this, LoginActivity.class));
                finish();
                return true;

            case R.id.action_exit:
                System.exit(0);

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_tabs_layout);


        //get data from shared prefs
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyProfile", MODE_PRIVATE);
        isLogin = sharedPreferences.getBoolean("isLogin", false);
        id = sharedPreferences.getInt("id", -1);

        //check if user is logged in
        if (!isLogin) {
            startActivity(new Intent(MainPosts.this, LoginActivity.class));
            finish();
        } else {
            //get styles
            JSON j = new JSON(this);
            j.fetch("getStyles", "getStyles.php");
        }



        initialiseTabHost(savedInstanceState);
        if (savedInstanceState != null) {
            mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab")); //set the tab as per the saved state
        }
        //mTabHost.offsetLeftAndRight(5);
    }


    protected void onSaveInstanceState(Bundle outState) {
        //outState.putString("tab", mTabHost.getCurrentTabTag()); //save the tab selected

        super.onSaveInstanceState(outState);
    }

    private void initialiseTabHost(Bundle args) {
        mTabHost = (TabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup();


        TabInfo tabInfo = null;
        MainPosts.addTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab1").setIndicator("Tab 1"), ( tabInfo = new TabInfo("Tab1", FragmentAllPosts.class, args)));

        this.mapTabInfo.put(tabInfo.tag, tabInfo);
        MainPosts.addTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab2").setIndicator("Tab 2"), ( tabInfo = new TabInfo("Tab2", FragmentFollowersPosts.class, args)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);

        // Default to first tab
        this.onTabChanged("Tab1");
        //
        mTabHost.setOnTabChangedListener(this);
        mTabHost.offsetLeftAndRight(5);

    }

    /**
     * @param activity
     * @param tabHost
     * @param tabSpec
     * @param tabInfo
     */
    private static void addTab(MainPosts activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
        // Attach a Tab view factory to the spec

        //set correct icon
        tabSpec.setIndicator(createTabIndicator(activity.getLayoutInflater(), tabHost, R.drawable.img_all_posts));
        tabSpec.setContent(activity.new TabFactory(activity));
        String tag = tabSpec.getTag();

        // Check to see if we already have a fragment for this tab, probably
        // from a previously saved state.  If so, deactivate it, because our
        // initial state is that a tab isn't shown.
        tabInfo.fragment = activity.getSupportFragmentManager().findFragmentByTag(tag);
        if (tabInfo.fragment != null && !tabInfo.fragment.isDetached()) {
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
            ft.detach(tabInfo.fragment);
            ft.commit();
            activity.getSupportFragmentManager().executePendingTransactions();
        }


        tabHost.addTab(tabSpec);
    }

    public static View createTabIndicator(LayoutInflater inflater, TabHost tabHost, int iconResource) {
        View tabIndicator = inflater.inflate(R.layout.tab_indicator, tabHost.getTabWidget(), false);
        ((ImageView) tabIndicator.findViewById(android.R.id.icon)).setImageResource(iconResource);
        return tabIndicator;
    }




    @Override
    public void onTabChanged(String tag) {
        TabInfo newTab = (TabInfo) this.mapTabInfo.get(tag);
        if (mLastTab != newTab) {
            FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
            if (mLastTab != null) {
                if (mLastTab.fragment != null) {
                    ft.detach(mLastTab.fragment);
                }
            }
            if (newTab != null) {
                if (newTab.fragment == null) {
                    newTab.fragment = Fragment.instantiate(this,
                            newTab.clss.getName(), newTab.args);
                    //ft.add(R.id.realtabcontent, newTab.fragment, newTab.tag);
                } else {
                    ft.attach(newTab.fragment);
                }
            }

            mLastTab = newTab;
            ft.commit();
            this.getSupportFragmentManager().executePendingTransactions();
        }
    }

    @Override
    public void getResult(String name, JSONObject jo) {
        if (name.equals("getStyles")) {
            if (jo != null) {
                try {
                    JSONArray jaStyles = jo.getJSONArray("styles");
                    SharedPreferences sharedPreferences = this.getSharedPreferences("Styles", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("styles", jaStyles.toString());
                    editor.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class TabInfo {
        private String tag;
        private Class clss;
        private Bundle args;
        private Fragment fragment;
        TabInfo(String tag, Class clazz, Bundle args) {
            this.tag = tag;
            this.clss = clazz;
            this.args = args;
        }

    }

    class TabFactory implements TabHost.TabContentFactory {

        private final Context mContext;

        /**
         * @param context
         */
        public TabFactory(Context context) {
            mContext = context;
        }

        /** (non-Javadoc)
         * @see android.widget.TabHost.TabContentFactory#createTabContent(java.lang.String)
         */
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
    }
}

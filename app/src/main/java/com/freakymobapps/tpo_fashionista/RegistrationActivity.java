package com.freakymobapps.tpo_fashionista;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by 633k on 1.12.2016.
 */
public class RegistrationActivity extends Activity implements CallBack{


    //views
    private Button btnRegister;
    private EditText etUsername;
    private EditText etPassword;
    private EditText etPasswordRep;
    private EditText etEmail;

    //variables
    private String username;
    private String password;
    private String passwordRep;
    private String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        init();


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get text from editText's to variables
                username = etUsername.getText().toString();
                password = etPassword.getText().toString();
                passwordRep = etPasswordRep.getText().toString();
                email = etEmail.getText().toString();

                //check if passwords are the same
                if (!password.equals(passwordRep)) {
                    Toast.makeText(RegistrationActivity.this, "Password's don't match", Toast.LENGTH_SHORT).show();
                    etPassword.setText("");
                    etPasswordRep.setText("");
                } else if (username.isEmpty() || password.isEmpty() || email.isEmpty()) { //check for empty strings
                    Toast.makeText(RegistrationActivity.this, "All fields must be filled", Toast.LENGTH_SHORT).show();
                } else {
                    //send data to server
                    JSON j = new JSON(RegistrationActivity.this);
                    try {
                        String data = getUrlEncodedText(username, password, email);
                        j.postProgress("register", "register.php", data);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private String getUrlEncodedText(String username, String password, String email) throws UnsupportedEncodingException {
        String data = URLEncoder.encode("username", "UTF-8")
                + "=" + URLEncoder.encode(username, "UTF-8");

        data += "&" + URLEncoder.encode("email", "UTF-8") + "="
                + URLEncoder.encode(email, "UTF-8");

        data += "&" + URLEncoder.encode("password", "UTF-8")
                + "=" + URLEncoder.encode(password, "UTF-8");
        return data;
    }

    private void init() {
        btnRegister = (Button) findViewById(R.id.btnRegister);
        etUsername = (EditText) findViewById(R.id.etUsernameReg);
        etPassword = (EditText) findViewById(R.id.etPasswordReg);
        etPasswordRep = (EditText) findViewById(R.id.etPasswordRegRep);
        etEmail = (EditText) findViewById(R.id.etEmail);
    }

    @Override
    public void getResult(String name, JSONObject jo) {
        if (name.equals("register") && jo != null) {
            try {
                int successful = jo.getInt("success");
                if (successful == 1) {
                    Toast.makeText(RegistrationActivity.this, "Registration is successful", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                    i.putExtra("username", username);
                    startActivity(i);
                    finish();
                } else Toast.makeText(RegistrationActivity.this, jo.getString("message"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

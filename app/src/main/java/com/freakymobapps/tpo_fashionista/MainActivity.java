package com.freakymobapps.tpo_fashionista;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.freakymobapps.tpo_fashionista.FragmentActivities.FragAddPost;
import com.freakymobapps.tpo_fashionista.FragmentActivities.FragmentAllPosts;
import com.freakymobapps.tpo_fashionista.FragmentActivities.FragmentFollowersPosts;
import com.freakymobapps.tpo_fashionista.FragmentActivities.MyProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 633k on 8.1.2017.
 */

public class MainActivity extends FragmentActivity implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener, CallBack {

    MyTabPageAdapter pageAdapter;
    private ViewPager mViewPager;
    private TabHost mTabHost;

    int myCount = 0;

    //data
    private int id;
    private boolean isLogin = false;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                mViewPager.setCurrentItem(0);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                Toast.makeText(MainActivity.this, "Here comes the about dialog!", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_logout:
                SharedPreferences sharedPreferences = this.getSharedPreferences("MyProfile", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("isLogin", false);
                editor.putInt("id", -1);
                editor.commit();

                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                return true;

            case R.id.action_exit:
                System.exit(0);

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_tabs_layout);


        //get data from shared prefs
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyProfile", MODE_PRIVATE);
        isLogin = sharedPreferences.getBoolean("isLogin", false);
        id = sharedPreferences.getInt("id", -1);

        //check if user is logged in
        if (!isLogin) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        } else {
            //get styles
            JSON j = new JSON(this);
            j.fetch("getStyles", "getStyles.php");
        }


        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setOffscreenPageLimit(4);

        // Tab Initialization
        initialiseTabHost();

        // Fragments and ViewPager Initialization
        vpInit();
    }

    void vpInit() {
        // Fragments and ViewPager Initialization
        List<Fragment> fragments = getFragments();
        pageAdapter = new MyTabPageAdapter(getSupportFragmentManager(), fragments);
        mViewPager.setAdapter(pageAdapter);
        mViewPager.addOnPageChangeListener(MainActivity.this);
    }

    // Method to add a TabHost
    private static void AddTab(MainActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec) {
        tabSpec.setContent(new MyTabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    // Manages the Tab changes, synchronizing it with Pages
    public void onTabChanged(String tag) {
        int pos = this.mTabHost.getCurrentTab();
        this.mViewPager.setCurrentItem(pos);

    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    // Manages the Page changes, synchronizing it with Tabs
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        int pos = this.mViewPager.getCurrentItem();
        this.mTabHost.setCurrentTab(pos);
    }

    @Override
    public void onPageSelected(int arg0) {
        if (arg0 == 2) {
            System.out.println("camera clicked");
            System.out.println("is true");
            //FragmentAddPost.isOnScreen = true;
            startActivityForResult(new Intent(this, AddPost.class), 1);
        }
    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<>();

        FragmentAllPosts f1 = FragmentAllPosts.newInstance();
        FragmentFollowersPosts f2 = FragmentFollowersPosts.newInstance();
        FragmentAddPost f3 = FragmentAddPost.newInstance(false);
        MyProfile f4 = MyProfile.newInstance();
        //TODO: add others here

        fList.add(f1);
        fList.add(f2);
        fList.add(f3);
        fList.add(f4);

        return fList;
    }

    // Tabs Creation
    private void initialiseTabHost() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();

        // TODO Put here your Tabs and add icons
        MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab1").setIndicator(createTabIndicator(this.getLayoutInflater(), mTabHost, R.drawable.img_all_posts)));
        MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab2").setIndicator(createTabIndicator(this.getLayoutInflater(), mTabHost, R.drawable.heartttt)));
        MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab3").setIndicator(createTabIndicator(this.getLayoutInflater(), mTabHost, R.drawable.camera)));
        MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab3").setIndicator(createTabIndicator(this.getLayoutInflater(), mTabHost, R.drawable.bla)));
        //MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab3").setIndicator(createTabIndicator(this.getLayoutInflater(), mTabHost, R.drawable.camera)));

        //MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab2").setIndicator("Tab2"));

        mTabHost.setOnTabChangedListener(this);
    }

    public static View createTabIndicator(LayoutInflater inflater, TabHost tabHost, int iconResource) {
        View tabIndicator = inflater.inflate(R.layout.tab_indicator, tabHost.getTabWidget(), false);
        ((ImageView) tabIndicator.findViewById(android.R.id.icon)).setImageResource(iconResource);
        return tabIndicator;
    }

    @Override
    public void getResult(String name, JSONObject jo) {
        if (name.equals("getStyles")) {
            if (jo != null) {
                try {
                    JSONArray jaStyles = jo.getJSONArray("styles");
                    SharedPreferences sharedPreferences = this.getSharedPreferences("Styles", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("styles", jaStyles.toString());
                    editor.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

package com.freakymobapps.tpo_fashionista;

import android.graphics.Bitmap;

/**
 * Created by 633k on 7.1.2017.
 */

public class Post {
    private int id;
    private String user;
    private String description;
    private String style;
    private int likes;
    private int hates;
    private int userlike;
    private int userhate;
    private int userpostID;
    private Bitmap pic;
    private Bitmap profilePic;
    private String date;

    public Post(int id, String user, String description, String style, int likes, int hates, int userlike, int userhate, int userpostID, Bitmap pic, Bitmap profilePic, String date) {
        this.id = id;
        this.user = user;
        this.description = description;
        this.style = style;
        this.likes = likes;
        this.hates = hates;
        this.userlike = userlike;
        this.userhate = userhate;
        this.userpostID = userpostID;
        this.pic = pic;
        this.profilePic = profilePic;
        this.date = date;
    }

    /*public Post(int id, String user, String description, String style, String likes, String hates, String userlike, String userhate, int userpostID, Bitmap pic, String date) {
        this.id = id;
        this.user = user;
        this.description = description;
        this.style = style;
        this.likes = likes;
        this.hates = hates;
        this.userlike = userlike;
        this.userhate = userhate;
        this.userpostID = userpostID;
        this.pic = pic;
        this.date = date;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getHates() {
        return hates;
    }

    public void setHates(int hates) {
        this.hates = hates;
    }

    public int getUserlike() {
        return userlike;
    }

    public void setUserlike(int userlike) {
        this.userlike = userlike;
    }

    public int getUserhate() {
        return userhate;
    }

    public void setUserhate(int userhate) {
        this.userhate = userhate;
    }

    public int getUserpostID() {
        return userpostID;
    }

    public void setUserpostID(int userpostID) {
        this.userpostID = userpostID;
    }

    public Bitmap getPic() {
        return pic;
    }

    public void setPic(Bitmap pic) {
        this.pic = pic;
    }

    public Bitmap getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Bitmap profilePic) {
        this.profilePic = profilePic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

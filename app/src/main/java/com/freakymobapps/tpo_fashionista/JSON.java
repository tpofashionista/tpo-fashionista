package com.freakymobapps.tpo_fashionista;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by 633k on 1.12.2016.
 */
public class JSON {
    private String url = "https://fashionista-tpo.000webhostapp.com/";
    private CallBack c;
    private Context context;

    public JSON(CallBack c) {
        this.c = c;
        this.context = (Context) c;
    }

    public JSON(Context context, CallBack c) {
        this.c = c;
        this.context = context;
    }


    public void fetch(String name, String command) {
        RequestTask t = new RequestTask();
        t.call = c;
        System.out.println(url + "api/" + command);
        t.execute(name, url + "api/" + command);
    }

    public void fetchWithProgress(String name, String command) {
        RequestTaskWithProgress t = new RequestTaskWithProgress();
        t.call = c;
        System.out.println(url + "api/" + command);
        t.execute(name, url + "api/" + command);
    }

    public void postProgress (String name, String command, String data) {
        PostTask t = new PostTask();
        t.call = c;
        System.out.println(url + "api/" + command);
        t.execute(name, url + "api/" + command, data);
    }

    public void post (String name, String command, String data) {
        PostTaskNoProgress t = new PostTaskNoProgress();
        t.call = c;
        System.out.println(url + "api/" + command);
        t.execute(name, url + "api/" + command, data);
    }


    /**
     * POST DATA WITH PROGRESS!!
    **/

    class PostTask extends AsyncTask<String, String, JSONObject> {

        String name = "";
        CallBack call;
        ProgressDialog pb;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                pb = new ProgressDialog(context);
                pb.setMessage("nalagam...");
                pb.setCancelable(false);
                pb.setCanceledOnTouchOutside(false);
                pb.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                    }
                });
                if (!pb.isShowing()) {
                    pb.show();
                }
            } catch (Exception e) {
            }

        }

        @Override
        protected JSONObject doInBackground(String... uri) {
            InputStream is = null;
            name = uri[0];
            JSONObject obj = null;
            //String parameters = "username="+uri[2]+"&password="+uri[3]+"&email="+uri[4];

            try {
                URL url = new URL(uri[1]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setReadTimeout(5000);
                con.setConnectTimeout(15000);
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                //con.setRequestProperty("Accept", "application/json");
                con.setRequestMethod("POST");
                con.setDoInput(true);
                con.setDoOutput(true);
                //con.connect();

                String data = uri[2];

                OutputStreamWriter printout = new OutputStreamWriter(con.getOutputStream());
                //printout.writeBytes(URLEncoder.encode(jData.toString(), "UTF-8"));
                printout.write(data);
                printout.flush();
                printout.close();

                //get data back from server
                int response = con.getResponseCode();
                System.out.println("response: " + response);

                if (response == HttpURLConnection.HTTP_OK) {
                    is = con.getInputStream();
                    String contentAsString = readIt(is);
                    System.out.println("back from server: " + contentAsString + "\n");
                    obj = new JSONObject(contentAsString);
                    return obj;
                } else {
                    System.out.println("response false: " + response);
                }


                return obj;
            } catch (IOException e) {
                e.printStackTrace();
            } /*catch (JSONException e) {
                e.printStackTrace();
            }*/ catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            call.getResult(name, result);
            if (pb.isShowing()) {
                pb.dismiss();
            }
        }
    }

    class PostTaskNoProgress extends AsyncTask<String, String, JSONObject> {

        String name = "";
        CallBack call;

        @Override
        protected JSONObject doInBackground(String... uri) {
            InputStream is = null;
            name = uri[0];
            JSONObject obj = null;
            //String parameters = "username="+uri[2]+"&password="+uri[3]+"&email="+uri[4];

            try {
                URL url = new URL(uri[1]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setReadTimeout(5000);
                con.setConnectTimeout(15000);
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                //con.setRequestProperty("Accept", "application/json");
                con.setRequestMethod("POST");
                con.setDoInput(true);
                con.setDoOutput(true);
                //con.connect();

                String data = uri[2];

                OutputStreamWriter printout = new OutputStreamWriter(con.getOutputStream());
                //printout.writeBytes(URLEncoder.encode(jData.toString(), "UTF-8"));
                printout.write(data);
                printout.flush();
                printout.close();

                //get data back from server
                int response = con.getResponseCode();
                System.out.println("response: " + response);

                if (response == HttpURLConnection.HTTP_OK) {
                    is = con.getInputStream();
                    String contentAsString = readIt(is);
                    System.out.println("back from server: " + contentAsString + "\n");
                    obj = new JSONObject(contentAsString);
                    return obj;
                } else {
                    System.out.println("response false: " + response);
                }


                return obj;
            } catch (IOException e) {
                e.printStackTrace();
            } /*catch (JSONException e) {
                e.printStackTrace();
            }*/ catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            call.getResult(name, result);
        }
    }

    /**
     * GET DATA!!
     **/


    class RequestTask extends AsyncTask<String, String, JSONObject> {
        String name = "";
        CallBack call;

        @Override
        protected JSONObject doInBackground(String... uri) {
            InputStream is = null;
            name = uri[0];
            JSONObject obj = null;
            try {

                URL url = new URL(uri[1]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(20000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.connect();

                int response = conn.getResponseCode();
                if (response == 200) {
                    is = conn.getInputStream();
                    String contentAsString = readIt(is);
                    obj = new JSONObject(contentAsString);
                    return obj;
                }
                return null;
                // Makes sure that the InputStream is closed after the app is
                // finished using it.
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            call.getResult(name, result);
        }
    }


    /**
     * GET DATA WITH PROGRESS!!
     **/



    class RequestTaskWithProgress extends AsyncTask<String, String, JSONObject> {
        String name = "";
        CallBack call;
        ProgressDialog pb;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                pb = new ProgressDialog(context);
                pb.setMessage("nalagam...");
                pb.setCancelable(false);
                pb.setCanceledOnTouchOutside(false);
                pb.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                    }
                });
                if (!pb.isShowing()) {
                    pb.show();
                }
            } catch (Exception e) {
            }
        }

        @Override
        protected JSONObject doInBackground(String... uri) {
            InputStream is = null;
            name = uri[0];
            JSONObject obj = null;
            try {

                URL url = new URL(uri[1]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(5000);
                conn.setConnectTimeout(20000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                if (response == 200) {
                    is = conn.getInputStream();
                    String contentAsString = readIt(is);
                    obj = new JSONObject(contentAsString);
                    return obj;
                }
                return null;

                // Makes sure that the InputStream is closed after the app is
                // finished using it.
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            call.getResult(name, result);
            if (pb.isShowing()) {
                pb.dismiss();
            }
        }
    }



    String readIt(InputStream stream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

}

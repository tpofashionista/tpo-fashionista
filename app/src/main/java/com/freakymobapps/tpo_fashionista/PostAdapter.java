package com.freakymobapps.tpo_fashionista;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.freakymobapps.tpo_fashionista.FragmentActivities.OtherProfile;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by 633k on 7.1.2017.
 */

public class PostAdapter extends BaseAdapter {


    private int userId;
    private int idPost;
    private int userpostID;

    private Context context;
    private ArrayList<Post> posts;
    public PostAdapter(Context context, ArrayList<Post> posts, int userId) {
        this.userId = userId;
        this.context = context;
        this.posts = posts;
    }

    @Override
    public int getCount() {
        return posts.size();
    }

    @Override
    public Object getItem(int i) {
        return posts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final Post post = (Post) getItem(i);

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.single_post, viewGroup, false);
        }

        ///init
        TextView tvUsername = (TextView) view.findViewById(R.id.row_username);
        TextView tvStyle = (TextView) view.findViewById(R.id.tvStyle);
        ImageView imgProfile = (ImageView) view.findViewById(R.id.row_prof_pic);
        ImageView imgPicture = (ImageView) view.findViewById(R.id.postPicture);
        final Button btnLike = (Button) view.findViewById(R.id.btnLike);
        final Button btnDislike = (Button) view.findViewById(R.id.btnDislike);
        final TextView tvNumLikes = (TextView) view.findViewById(R.id.tvNumLikes);
        final TextView tvNumDislikes = (TextView) view.findViewById(R.id.tvNumDislikes);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);

        //set values
        tvUsername.setText(post.getUser());
        tvStyle.setText(post.getStyle());
        imgProfile.setImageBitmap(post.getProfilePic());
        imgPicture.setImageBitmap(post.getPic());
        tvNumLikes.setText(post.getUserlike()+"");
        tvNumDislikes.setText(post.getUserhate()+"");
        tvDescription.setText(post.getDescription());


        if (post.getUserlike() == 1){
            btnLike.setEnabled(false);
        } else if (post.getUserhate() == 1) {
            btnLike.setEnabled(false);
        } else {
            btnLike.setEnabled(true);
            btnDislike.setEnabled(true);
        }


            btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strLikes = tvNumLikes.getText().toString();
                String strDislikes = tvNumDislikes.getText().toString();
                int intLikes = Integer.parseInt(strLikes);
                int intDislikes = Integer.parseInt(strDislikes);
                int idPost = posts.get(i).getUserpostID();

                post.setLikes(intLikes+1);
                if (!btnDislike.isEnabled()) {
                    post.setHates(intDislikes-1);
                    tvNumDislikes.setText(String.valueOf(intDislikes-1));
                }
                tvNumLikes.setText(String.valueOf(intLikes+1));
                //new Like().execute();
                btnLike.setEnabled(false);
                //btnLike.setBackgroundResource(R.drawable.like1);
                btnDislike.setEnabled(true);
                //btnDislike.setBackgroundResource(R.drawable.dislike);

                Toast.makeText(context, "LIKE", Toast.LENGTH_SHORT).show();

                JSON j = new JSON(context, new CallBack() {
                    @Override
                    public void getResult(String name, JSONObject jo) {

                    }
                });
                j.post("likePost", "like.php", "ID_up=" + userId + "&ID_objava=" + post.getId());
            }
        });

        btnDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strLikes = tvNumLikes.getText().toString();
                String strDislikes = tvNumDislikes.getText().toString();
                int intLikes = Integer.parseInt(strLikes);
                int intDislikes = Integer.parseInt(strDislikes);
                int idPost = posts.get(i).getUserpostID();

                post.setHates(intDislikes+1);
                if (!btnLike.isEnabled()) {
                    post.setLikes(intLikes-1);
                    tvNumLikes.setText(String.valueOf(intLikes-1));
                }
                tvNumDislikes.setText(String.valueOf(intDislikes+1));
                //new Like().execute();
                btnDislike.setEnabled(false);
                //btnLike.setBackgroundResource(R.drawable.like1);
                btnLike.setEnabled(true);
                //btnDislike.setBackgroundResource(R.drawable.dislike);

                Toast.makeText(context, "DIS LIKE", Toast.LENGTH_SHORT).show();

                JSON j = new JSON(context, new CallBack() {
                    @Override
                    public void getResult(String name, JSONObject jo) {

                    }
                });
                j.post("dislikePost", "hate.php", "ID_up=" + userId + "&ID_objava=" + post.getId());
            }
        });

        tvUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.getContext().startActivity(new Intent(context, OtherProfile.class).putExtra("otherID", post.getUserpostID()));
            }
        });

        return view;
    }
}

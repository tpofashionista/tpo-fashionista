package com.freakymobapps.tpo_fashionista;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by 633k on 1.12.2016.
 */
public class LoginActivity extends Activity implements CallBack {

    //views
    private Button btnLogin;
    private Button btnRegister;
    private EditText etUsername;
    private EditText etPassword;


    //variables
    private String username;
    private String password;
    private int id; //get id from server


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log);

        //initialize components
        init();

        Intent i = getIntent();
        String usernameFromReg = "";
        usernameFromReg = i.getStringExtra("username");
        if (usernameFromReg != null) etUsername.setText(usernameFromReg);
    }


    private void init() {
        btnRegister = (Button) findViewById(R.id.btnRegisterLog);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        etUsername = (EditText) findViewById(R.id.etUsernameLog);
        etPassword = (EditText) findViewById(R.id.etPasswordLog);


        //button register clicked: go to registerActivity
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
            }
        });

        //button login clicked
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = etUsername.getText().toString();
                password = etPassword.getText().toString();

                //check if something is empty
                if (username.isEmpty() || password.isEmpty()) Toast.makeText(LoginActivity.this, "Fill all fields", Toast.LENGTH_SHORT).show();
                else {
                    //send data to server
                    JSON j = new JSON(LoginActivity.this);
                    try {
                        String data = getUrlEncodedText(username, password);
                        j.postProgress("login", "login.php", data);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    @Override
    public void getResult(String name, JSONObject jo) {
        if (name.equals("login") && jo != null) {
            try {
                int successful = jo.getInt("success");
                if (successful == 1) {
                    id = jo.getInt("id");
                    Toast.makeText(LoginActivity.this, "Login is successful", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    i.putExtra("id", id);
                    i.putExtra("isLogin", true);

                    //set shared preferences
                    SharedPreferences sharedPreferences = this.getSharedPreferences("MyProfile", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("isLogin", true);
                    editor.putInt("id", id);
                    editor.commit();


                    startActivity(i);
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private String getUrlEncodedText(String username, String password) throws UnsupportedEncodingException {
        String data = URLEncoder.encode("username", "UTF-8")
                + "=" + URLEncoder.encode(username, "UTF-8");
        data += "&" + URLEncoder.encode("password", "UTF-8")
                + "=" + URLEncoder.encode(password, "UTF-8");
        return data;
    }
}
